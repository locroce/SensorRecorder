package de.tonifetzer.sensorrecorder;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.nstant.in.cbor.CborException;
import de.tonifetzer.sensorrecorder.sender.Sender;
import de.tonifetzer.sensorrecorder.sensors.Entry;
import de.tonifetzer.sensorrecorder.sensors.Logger;
import de.tonifetzer.sensorrecorder.sensors.MySensor;
import de.tonifetzer.sensorrecorder.sensors.PhoneSensors;

public class MainActivity extends AppCompatActivity {

    private static final int HISTORY_SIZE = 1500;

    //ui in main layout
    private View mainView;
    private ToggleButton recButton;
    private TextView txtViewFilename;

    private TextInputEditText inputUser;
    private Button confirmUser;
    private Button sendFiles;

    private TextInputEditText inputPosition;
    private Button confirmPosition;

    private PhoneSensors phoneSensors;
    private final Logger dataLogger = new Logger(this);
    private final Sender sender = new Sender(this);

    private String appUser;
    private String appPosition;
    private String TAG = MainActivity.class.getCanonicalName();

    ProgressDialog progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sender.start();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading...");

        //this is super ugly. normally we would use fragments to switch ui
        mainView = getLayoutInflater().inflate(R.layout.activity_main, null);
        setContentView(mainView);

        //this is a small hack to get a static context
        MainActivity.context = getApplicationContext();

        //init the sensors of the phone
        phoneSensors = new PhoneSensors(this);

        //find ui elements in main
        inputUser = findViewById(R.id.user_field);
        confirmUser = findViewById(R.id.confirm_user);
        inputPosition = findViewById(R.id.input_position);
        confirmPosition = findViewById(R.id.confirm_position);
        sendFiles = findViewById(R.id.button_send);
        recButton = findViewById(R.id.toggleButton);
        txtViewFilename = findViewById(R.id.textViewFilename);

        //set the listener
        phoneSensors.setListener(new MySensor.SensorListener(){
            @Override public void onData(final String csv) {}
            @Override
            public void onData(final Entry entry) throws CborException {
                addDataToFile(entry);
            }
        });

        sendFiles.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                    //TODO list files in directory and send them
                    // TODO upload single file iter list files
                    //Log.d(TAG, "onClick: " + Arrays.asList(dataLogger.getFolder()
                      //      .getFolder().list()));
                try {
                    fileSend();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        confirmUser.setOnClickListener(new CompoundButton.OnClickListener() {

            @Override
            public void onClick(View v) {
                appUser = inputUser.getText().toString();
                Toast.makeText(MainActivity.this, "User set to " + appUser,
                        Toast.LENGTH_SHORT).show();
                dataLogger.setAndroidUser(appUser);
            }
        });

        confirmPosition.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                appPosition = inputPosition.getText().toString();
                Toast.makeText(MainActivity.this, "Position set to " + appPosition,
                        Toast.LENGTH_LONG).show();
                dataLogger.setAndroidPosition(appPosition);
            }
        });

        recButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    buttonView.setBackgroundColor(Color.parseColor("#FFCC0000"));
                    startRecording();
                }
                else {
                    buttonView.setBackgroundColor(Color.parseColor("#FF669900"));
                    try {
                        stopRecording();
                    } catch (CborException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void startRecording() {
        phoneSensors.onResume(this);
        Log.d(TAG, "startRecording: User " + dataLogger.getAndroidUser());
        Log.d(TAG, "startRecording: Position " + dataLogger.getAndroidPosition());
        /*dataLogger.start(phoneSensors.getAll());*/
        List all = new ArrayList<Sensor>();
        all.add(phoneSensors.getAccelero());
        all.add(phoneSensors.getGyro());
        all.add(phoneSensors.getLinear());
        dataLogger.start(all);

        String path = dataLogger.getFile().getAbsolutePath();
        txtViewFilename.setText(path.substring(path.length()-17));
//        all = new ArrayList<Sensor>();
    }

    private void stopRecording() throws CborException {
        phoneSensors.onPause(this);
        dataLogger.stop();
    }

    private void addDataToFile(Entry entry) throws CborException {

        dataLogger.addEntry(entry);

        runOnUiThread(new Runnable() {
            @Override public void run() { }
        });

    }

    //This is also part of the hack to get a static context
    private static Context context;

    public static Context getAppContext() {
        return MainActivity.context;
    }

    public void fileSend() throws Exception {
        for (File f : dataLogger.getFolder().getFolder().listFiles()) {
//            Log.d("FileSend", "startRecording: " + f);
            sender.upload(f, progressDialog);
        }
    }
}
