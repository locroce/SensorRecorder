package de.tonifetzer.sensorrecorder.sensors;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import co.nstant.in.cbor.CborException;

/**
 * all available sensors
 * and what to do within one class
 *
 */
public class PhoneSensors extends MySensor implements SensorEventListener{

    private SensorManager sensorManager;
	private Sensor accelero;
	private Sensor gyro;
	private Sensor linear;

    private int DELAY = 10000; // 100Hz in µs

	/** ctor */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
	public PhoneSensors(final Activity act){
		// fetch the sensor manager from the activity
        sensorManager = (SensorManager) act.getSystemService(Context.SENSOR_SERVICE);

		// try to get each sensor
		//all = sensorManager.getSensorList(Sensor.TYPE_ALL);
		accelero = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		gyro = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		linear = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        // dump sensor-vendor info to file
		// dumpVendors(act);
	}

	private final char NL = '\n';

	/** Write Vendors to file */
	@RequiresApi(api = Build.VERSION_CODES.KITKAT)
	private void dumpVendors(final Activity act) {

		final DataFolder folder = new DataFolder(act, "sensorOutFiles");
		final File file = new File(folder.getFolder(), "vendors.txt");

		try {

			final FileOutputStream fos = new FileOutputStream(file);
			final StringBuilder sb = new StringBuilder();

			// constructor smartphone details
			sb.append("[Device]").append(NL);
			sb.append("\tModel: ").append(Build.MODEL).append(NL);
			sb.append("\tAndroid: ").append(Build.VERSION.RELEASE).append(NL);
			sb.append(NL);

			// construct sensor details
			/*for (Sensor s: all) {
				dumpSensor(sb, s);
			}*/
			dumpSensor(sb, accelero);
			dumpSensor(sb, gyro);
			dumpSensor(sb, linear);
			// write
			fos.write(sb.toString().getBytes());
			fos.close();

		}catch (final IOException e) {
			throw new RuntimeException(e);
		}

	}

	/** dump all details of the given sensor into the provided stringbuilder */
	private void dumpSensor(final StringBuilder sb, final Sensor sensor) {
		sb.append("[Sensor]").append(NL);

		if (sensor != null) {
			sb.append("\tVendor: ").append(sensor.getVendor()).append(NL);
			sb.append("\tName: ").append(sensor.getName()).append(NL);
			sb.append("\tVersion: ").append(sensor.getVersion()).append(NL);
			sb.append("\tMinDelay: ").append(sensor.getMinDelay()).append(NL);
			//sb.append("\tMaxDelay: ").append(sensor.getMaxDelay()).append(NL);
			sb.append("\tMaxRange: ").append(sensor.getMaximumRange()).append(NL);
			sb.append("\tPower: ").append(sensor.getPower()).append(NL);
			//sb.append("ReportingMode: ").append(sensor.getReportingMode()).append(NL);
			sb.append("\tResolution: ").append(sensor.getResolution()).append(NL);
			sb.append("\tType: ").append(sensor.getType()).append(NL);
		} else {
			sb.append("\tnot available!\n");
		}
		sb.append(NL);
	}

    @Override
    public void onSensorChanged(SensorEvent event) {

		long now = System.currentTimeMillis();
//		Log.d("PhoneSensors", "onSensorChanged: " + event.sensor.getType());

		try {
			if (event.values.length == 1) {
				listener.onData(new Entry(now, event.values[0], 0, 0,
						event.sensor.getType()));
			}
			else if (event.values.length == 2) {
				listener.onData(new Entry(now, event.values[0], event.values[1], 0,
						event.sensor.getType()));
			} else {
				listener.onData(new Entry(now, event.values[0], event.values[1], event.values[2],
						event.sensor.getType()));
			}
		} catch (CborException e) {
			e.printStackTrace();
		}
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// nothing to-do here
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
	@Override
    public void onResume(final Activity act) {
		// attach as listener to each of the available sensors
		/*for (Sensor s: all) {
			registerIfPresent(s, DELAY);
		}*/
		registerIfPresent(accelero, DELAY);
		registerIfPresent(gyro, DELAY);
		registerIfPresent(linear, DELAY);
    }

	@RequiresApi(api = Build.VERSION_CODES.KITKAT)
	private void registerIfPresent(final Sensor sens, final int delay) {
		if (sens != null) {
			sensorManager.registerListener(this, sens, delay);
			Log.d("PhoneSensors", "added sensor " + sens.toString());
		} else {
			Log.d("PhoneSensors", "sensor not present. skipping");
		}
	}

    @Override
    public void onPause(final Activity act) {
		// detach from all events
		sensorManager.unregisterListener(this);
    }

/*    public List<Sensor> getAll() {
		return all;
	}*/

	public Sensor getAccelero() {
		return accelero;
	}

	public Sensor getGyro() {
		return gyro;
	}

	public Sensor getLinear() {
		return linear;
	}

}
