package de.tonifetzer.sensorrecorder.sensors;

import android.app.Activity;

import co.nstant.in.cbor.CborException;

/**
 * base-class for all Sensors
 */
public abstract class MySensor {

	/** listen for sensor events */
	public interface SensorListener {

		void onData(final String csv);

		/** received data from the given sensor */
        void onData(final Entry entry) throws CborException;

	}

	/** the listener to inform (if any) */
	protected SensorListener listener = null;


	/** start the sensor */
    public abstract void onResume(final Activity act);

	/** stop the sensor */
	public abstract void onPause(final Activity act);

	/** attach the given listener to the sensor */
	public void setListener(final SensorListener listener) {this.listener = listener;}

}
